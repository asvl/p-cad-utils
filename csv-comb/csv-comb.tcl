#!/usr/bin/tclsh
#
# Description:
# 1. Combine P-CAD CSV output BOM by some field (e.g. RefDes)
# 2. Merge several fields to one
#
# Usage:
#	csv-comb.tcl --help
#
# (c) 2010 Sergey Alyoshin <alyoshin.s@gmail.com>
# Licensed under WTFPL

package require csv

set g_comb_by		"RefDes"	;# Combine elements of this field

# TODO: merge from nested list: {{} {}} to first element of each nested list.

					;# Merge this field in given order to one field
#set g_merge_fields	{"Value" "ComponentName" "PatternName"}
#set g_merge_fields	{"Value" "Voltage" "Material"}
set g_merge_fields	{"Value"}
set g_comb_delim	", "		;# Delimiter for combined elements
set g_comb_end		"."		;# End char for combined elements
set g_not_comb_by	"Count"		;# Not combine by this field,
					;# even if equal value (but not empty)
set g_err_nr 0
# Print error message
proc printe {arg} {
	global g_err_nr

	puts stderr "Error: $arg"
	incr g_err_nr
}

proc printd {arg} {
	global g_debug

	if {$g_debug != 0} {
		puts stderr "Debug: $arg"
	}
}

proc output {arg} {

	# Clenup
	for {set i 0} {$i < [llength $arg]} {incr i} {
		if {[lindex $arg $i] == "\""} {
			set arg [lreplace $arg $i $i {}]
		}
	}

	set arg [merge $arg $::g_merge_fields_indexes]

	puts [::csv::join $arg]
}

proc merge {lst indexes} {
	# Merge fields in lst by indexes

	# Form new field
	set merged {}
	for {set i 0} {$i < [llength $indexes]} {incr i} {
		set j [lindex $indexes $i]
		if {[lindex $lst $j] != {}} {
			lappend merged [lindex $lst $j]
		}
	}

	# Form new list
	set out {}
	for {set i 0} {$i < [llength $lst]} {incr i} {
		set j [lsearch -exact $indexes $i]
		if {$j < 0} {
			# Not in merged list
			lappend out [lindex $lst $i]
		} elseif {$j == 0} {
			lappend out [join $merged]
		}
	}

	return $out
}


# Main

# Parse arguments
set g_skip_lines	0
set g_debug		0
set g_case_compare	0
for {set i 0} {$i < $argc } {incr i} {
	set arg [lindex $argv $i]

	if {[scan $arg "--skip=%d" v] == 1} {
		if {$v > 0} {
			set g_skip_lines $v
		}
	}

	if {$arg == "--debug"} {
		set g_debug 1
	}

	if {$arg == "--case"} {
		set g_case_compare 1
	}

	if {$arg == "--help" || $arg == "-h"} {
		puts stderr "Usage: csv-comb.tcl \[arguments\] <in.csv >out.csv\n\
			Where arguments are:\n\
			--skip=n\tskip first n lines (if header are not on top)\n\
			--case\t\tcase sensitive compare\n\
			--debug\tprint debug information\n\
			--help|-h\tprint this help"
		return 0
	}
}

if {$g_case_compare} {
	set compare_arg {}
	printd "case sensitive compare"
} else {
	set compare_arg {-nocase}
	printd "case insensitive compare"
}

set line_nr 0
set file_line_nr 0
set empty_line_nr 0

puts stderr "Reading from stdin..."

# Skip first lines
for {set i 0} {$i < $g_skip_lines} {incr i} {
	if ![eof stdin] {
		incr file_line_nr
		if {[gets stdin l] < 0} {
			break
		} else {
			puts $l		;# Put line without changes
		}
	}
} 
if {$g_skip_lines > 0} {
	printd "skiping first $g_skip_lines lines"
}

# Do main job
while ![eof stdin] {

	incr file_line_nr

	if {[gets stdin l] < 0} {
		break
	}

	# Skip empty lines
	if {0 == [string length [string trim $l]]} {
		incr empty_line_nr
		continue
	}

	incr line_nr

	if ![::csv::iscomplete $l] {

		if {$line_nr == 1} {
			printe "header incomplete"
			exit 1
		} else {
			printe "incomplete line $file_line_nr"
			continue
		}
	}

	if {$line_nr == 1} {
		# This is header, remember index for Value
 		printd "header: $l"

		set ll [::csv::split $l]
		set col [llength $ll]
		set comb_index [lsearch -exact $ll $g_comb_by]
		set not_comb_index [lsearch -exact $ll $g_not_comb_by]

		if {$comb_index == -1} {
			printe "string $g_comb_by not found,\
				maybe you should use --skip"
			exit 1
		}
		if {$not_comb_index == -1} {
			printe "string $g_not_comb_by not found,\
				maybe you should use --skip"
			exit 1
		}

		printd "found $g_comb_by at $comb_index from $col columns"
		printd "found $g_not_comb_by at $not_comb_index from $col columns"

		# ll_prev init
		set ll_prev {}
		foreach none $ll {
			lappend ll_prev ""
		}

		# Search merge fields indexes
		set g_merge_fields_indexes {}
		for {set i 0} {$i < [llength $g_merge_fields]} {incr i} {
			set j [lsearch -exact $ll [lindex $g_merge_fields $i]]
			if {$j < 0} {
				printe "can't find field [lindex $g_merge_fields $i] index to merge"
				exit 2
			}
			lappend g_merge_fields_indexes $j
		}

		set ll [merge $ll $g_merge_fields_indexes]

		# Output header
		printd "$file_line_nr (header):"
		puts [::csv::join $ll]

		continue
	}

	set ll [::csv::split $l]

	# Compare
	for {set i 0} {$i < $col} {incr i} {
		# Skip null field
		if {[lindex $ll $i] == "" || [lindex $ll $i] == "\""} {
			printd "skip empty field $i at $file_line_nr"
			continue
		}

		if {$i != $comb_index} {
			if {[eval string compare $compare_arg {[lindex $ll $i]} {[lindex $ll_prev $i]}]} {
				printd "not equal field $i at line $file_line_nr: [lindex $ll $i] AND [lindex $ll_prev $i]"
				break
			} elseif {$i == $not_comb_index} {
				# Field is equal but should not be combined
				printd "NOT combine line $file_line_nr via $g_not_comb_by"
				break
			}
		}
	}

	if {$i != $col} {
		# Current line is different
		# Output previous (combined) line puts [::csv::joinlist $ll_prev]
		printd "line $file_line_nr is different"

		# Add end char
 		if {[lindex $ll_prev $comb_index] != ""} {
			set ll_prev [lreplace $ll_prev $comb_index $comb_index \
				[lindex $ll_prev $comb_index]$g_comb_end]
		}

		printd "$file_line_nr prev:"
		output $ll_prev

		set ll_prev $ll
	} else {
		printd "combine line $file_line_nr:\
			[lindex $ll_prev $comb_index]$g_comb_delim[lindex $ll $comb_index]"
		# Combine current line with previous
		set ll_prev [lreplace $ll_prev $comb_index $comb_index \
			[lindex $ll_prev $comb_index]$g_comb_delim[lindex $ll $comb_index]]
	}
}

# Output last line
if {$line_nr > 1} {
	# Add end char
 	if {[lindex $ll_prev $comb_index] != ""} {
		set ll_prev [lreplace $ll_prev $comb_index $comb_index \
			[lindex $ll_prev $comb_index]$g_comb_end]
	}
	printd "$file_line_nr:"
	output $ll_prev
}

# Report
puts -nonewline stderr "Processed $line_nr lines "
if {$g_err_nr != 0} {
	puts -nonewline stderr "with $g_err_nr errors"
}
puts stderr ""			;# New line
if {$g_skip_lines != 0} {
	puts stderr "\t$g_skip_lines top lines skipped"
}
if {$empty_line_nr != 0} {
	puts stderr "\t$empty_line_nr empty lines skipped"
}

