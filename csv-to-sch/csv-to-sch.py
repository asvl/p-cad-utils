#!/usr/bin/python

# Generate P-CAD ASCII .sch file with pins from .csv file.
# Pins then can be copied to element.
#
# NOTE: at file loading P-CAD must produce no warnings, or something would
#       be lost.
#
# 1) .csv file should have pinDes and pinName columns and first row as header.
# 2) If .csv file has no pinName, then pinDes value would be used as pinName.
# 3) If .csv file has pinExtName field or filed(s) with no header they would be
#    assigned as pinExtName_<pinDes> attribute and place as pinName (pinName
#    would not be shown).
# 4) Empty row in .csv file will produce new pins column in .sch file.
# 5) pinDes value can be comma separated.
#
#
# (c) 2011, 2013, 2017 Sergey Alyoshin <alyoshin.s@gmail.com>
# Licensed under WTFPL

from __future__ import print_function
import csv
import sys

# Configuration
composed_pin_name = False

pin_name_max = 20	# P-CAD limitation

r = csv.DictReader(sys.stdin, restkey = 'withoutHeader')

def print_sch_design_header():

	print(	'(schematicDesign ""\n'
		' (schDesignHeader\n'
		'  (workspaceSize ' + str(workspace_x_mm) + ' mm ' + str(workspace_y_mm) + ' mm)\n'
		'  (gridDfns (grid "100.0"))\n  (designInfo)\n'
		' )\n'
		' (sheet "Sheet1" (sheetNum 1)\n'
		'  (fieldSetRef "(Default)")')

def print_pin(num, des, name, x, y, attr = None):

	des = str.strip(des)
	name = str.strip(name)

	print(	'  (pin (pinNum ' + str(num) + ') (pt ' + str(x) + ' ' + str(y) + ')'
		'(rotation 180.0)\n')
	if (attr == None):
		print('   (pinDisplay (dispPinName True))\n')
	else:
		if (composed_pin_name):
			name = des + ':' + name
		print('   (pinDisplay (dispPinName False))\n')

	if (len(name) > pin_name_max):
		print('Error: pinName: ' + name + ' is more than ' + str(pin_name_max) +
			' chars, pinDes: ' + des,
			file = sys.stderr)
		exit(1)


	print(	'   (pinDes (text (pt ' + str(x - 65) + ' ' + str(y) + ') "' + des + '" '
		'(textStyleRef "(PinStyle)") (isFlipped True) (extent 0.0 mm 0.0 mm)))\n'
		'   (pinName (text (pt ' + str(x + 20) + ' ' + str(y) + ') "' + name + '" '
		'(textStyleRef "(PinStyle)") (isFlipped True) (justify Right) (extent 0.0 mm 0.0 mm)))\n'
		'   (defaultPinDes "' + des + '")\n'
		'  )')

	if (attr != None):
		if (isinstance(attr, list)):
			# Remove empty elements in list
			tmp = []
			for i in range(len(attr)):
				if (str.strip(attr[i]) != ''):
					tmp.append(str.strip(attr[i]))
			attr = '/'.join(tmp)

		print('  (attr "pinExtName_' + des + '" "' + attr + '" (pt ' + str(x + 20) + ' ' + str(y) +
			') (isVisible True) (justify Left) (textStyleRef "(PinStyle)"))')

def print_port(port, x, y):

	print('    (port (pt ' + str(x) + ' ' + str(y) + ')'
		'(portType NoOutline_Sgl_Vert) (portPinLength PortPinShort) (netNameRef "' + port + '"))')

# main

produce_ports = 0

# Check args and print help
for arg in sys.argv:
	if (arg == '-p' or arg == '--port'):
		produce_ports = 1

	if (arg == '-h' or arg == '--help'):
		print('Thin script produce P-CAD ASCII schematic file to stdout.\n' +
			'Usage:\n' +
			'-h, --help\tthin help\n' +
			'-p, --port\tcreate ports from .csv on stdin with "port" column\n' +
			'none\t\tcreate pins from .csv on stdin with "pinDes", "pinName"\n' +
			'\t\t\t(or "pinExtName") columns')
		exit(0)

if (produce_ports):
	print('Generating ports to stdout\n', file = sys.stderr)
else:
	print('Generating pins to stdout\n', file = sys.stderr)

x_step = 1000		# mils
y_step = -200		# mils

x_start = 200.0 + 300	# Starting x-coord of pins, 300 is default pin length, mils
y_start = 100.0		# Starting y-coord of pins, mils

a3_workspace_x_mm = 420.0
a3_workspace_y_mm = 297.0
a2_workspace_x_mm = 594.0
a2_workspace_y_mm = 420.0
mils_in_mm = 1000/25.4

workspace_x_mm = a2_workspace_x_mm
workspace_y_mm = a2_workspace_y_mm
x_max = workspace_x_mm * mils_in_mm
y_max = workspace_y_mm * mils_in_mm

print(	'ACCEL_ASCII ""\n'
	'(asciiHeader\n (asciiVersion 3 0)\n (fileUnits Mil)\n'
	')')

x = x_start
if (y_step < 0):
	y_start = (y_max - y_start)//abs(y_step)*abs(y_step)

y = y_start

num = 1
skiped_num = 0

# Produce ports
if (produce_ports):
	ports = []
	for row in r:
		try:
			port = row['port']
		except:
			port = None

		if (port == None or port == ''):
			print('Warning: skipping row with no port: ' +
				str(row), file = sys.stderr)
			skiped_num = skiped_num + 1
			continue

		ports = ports + [str.strip(port)]

	print('(netlist "Netlist_1"')
	for port in ports:
		print('    (net "' + port + '")');
	print(')')

	print_sch_design_header()

	for port in ports:
		print_port(port, x, y)

		y = y + y_step
		if (y >= y_max or y <= 0):
			y = y_start
			x = x + x_step
			if (x >= x_max):
				print('Error: workspace limit reached on pin ' +
					str(num), file = sys.stderr)
				exit(1)
		num = num + 1

	print(	' )\n)')

	print('Done with ' + str(num - 1) +
		' ports (skipped ' + str(skiped_num) + ' rows)',
		file = sys.stderr)
	exit(0)

# Produce pins
print_sch_design_header()

skipping_prev = 0

for row in r:
	try:
		pin_des = row['pinDes']
	except:
		pin_des = None
	
	try:
		pin_name = row['pinName']
	except:
		print('Warning: can\'t get pinName, using pinDes (' + str(pin_des) + ') as pinName',
				file = sys.stderr)
		pin_name = pin_des

	if (pin_des == None or pin_des == ''):
		print('Warning: skipping row with no pinDes: ' + str(row), file = sys.stderr)
		skiped_num = skiped_num + 1

		# Change to next column if row is
		if (not skipping_prev): 
			x = x + x_step
			y = y_start

		skipping_prev = 1

		continue

	try:
		attr = row['pinExtName']
	except:
		try:
			attr = row['withoutHeader']
		except:
			attr = None

	skipping_prev = 0

	# Check if pin_des contain several comma separated chars
	pin_des_list = pin_des.split(',')

	if (len(pin_des_list) == 0):
		pin_des_list = [pin_des] # One element list for cycle

	
	for pin_des in pin_des_list:
		print_pin(num, pin_des, pin_name, x, y, attr)

		y = y + y_step
		if (y >= y_max or y <= 0):
			y = y_start
			x = x + x_step
			if (x >= x_max):
				print('Error: workspace limit reached on pin ' + str(num),
					file = sys.stderr)
				exit(1)
		num = num + 1

print(	' )\n)')

print('Done with ' + str(num - 1) + ' pins (skipped ' + str(skiped_num) + ' rows)',
		file = sys.stderr)
exit(0)
