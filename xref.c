/* Update 'Xref' attribute of sheet connectors in P-Cad schematic.
	(c) 2012 Sergey Alyoshin <alyoshin.s@gmail.com>
*/

/* NOTE: can't place info point in circuit */

/*	TODO:

	* To sort XRef by sheet nums, get sheets in sequence by they order,
	  not by they id.

	* If offpage connector net exist not only in offpage connector, then
	  point at that sheet and warn a user.

	* Rotate Xref attr text if pin is rotated (flipped)
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <conio.h>
#include <Dbx32.h>	/* P-Cad interface */

/* Configuration */
const char ref_des_prefix[DBX_MAX_NAME_LEN] = "OFF";	/* "" for any */
const char comp_type[DBX_MAX_NAME_LEN] = ""; 
const char sheet_list_attr_def[] = "Xref";
const char delimeter[] = ", ";

struct sheet_conn_item {
	char *ref_des;
	char *net;
	long int sheet_num;
	TItem item;
};

struct sheet_conns {
	struct sheet_conn_item *items;
	unsigned int nums;
};

void print_help(void)
{
	printf("Update attribute of sheet connectors in P-Cad schematic\n"
		"\n"
		"Usage:\n"
		"-h	print this help and exit\n"
		"-H	highlight items with warning\n"
		"<attr>	use <attr> attribute for sheet numbers\n"
		"	(%s by default)\n", sheet_list_attr_def);
}

void do_exit(int code)
{
	fprintf(stderr, "\n*** Press key to close ***\n");
	getch();
	exit(code);
}

int get_ref_des_attr_type(DbxContext *context, const char *ref_des,
		const char *attr_type, TAttribute *attr)
{
	if (DBX_OK != TGetFirstSymAttribute(context, ref_des, attr))
		return 0;

	do {
		if (!strcmp(attr_type, attr->type))
			return 1;
	} while (DBX_OK == TGetNextSymAttribute(context, attr));

	return 0;
}

int main(int argc, char *argv[])
{
	DbxContext context;
	TLayer layer;
	TItem item;
	TAttribute attr;
	TComponent comp;
	unsigned int i, j, prev, ref_des_prefix_len, attr_buff_pos;
	char *ref_des;
	struct sheet_conns found;
	struct sheet_conn_item *scip;
	unsigned int warn_nums = 0, no_match_nums = 0;
	void *p;
	const char *sheet_list_attr = sheet_list_attr_def;
	char attr_buff[2*DBX_MAX_ATTRIBUTE_LEN];
	int do_highlight = 0;

	
	found.nums = 0;
	found.items = NULL;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] != '-') {
			sheet_list_attr = argv[i];
		}
		if (!strcmp(argv[i], "-H")) {
			do_highlight = 1;
		}
		if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
			print_help();
			do_exit(0);
		}
	}

	fprintf(stderr, "Updating '%s' attribute of sheet connectors\n\n",
			sheet_list_attr);

	if (DBX_OK != TOpenDesign(DBX_LANGUAGE, DBX_VERSION, "sch", &context)) {
		printf("Can't open\n");
		do_exit(-1);
	}

	/* TODO: 2. Sort get sheets in sequence by they order,
	 * not by they id */

	if (DBX_OK != TGetFirstLayer(&context, &layer)) {
		printf("Can't get first sheet\n");
		do_exit(-2);
	}

	ref_des_prefix_len = strlen(ref_des_prefix);

	do {
		if (DBX_OK != TGetFirstLayerItem(&context, layer.layerId, &item))
			continue;

		do {
			if (item.itemType != DBX_SYMBOL)
				continue;
			ref_des = item.symbol.refDes;

			if (DBX_OK != TGetCompByRefDes(&context, ref_des, &comp)) {
#if 0
				printf("Warning: skipping %s item: "
					"can't get component\n", ref_des);
#endif
				continue;
			}
			if (comp.connectionType != DBX_COMPONENT_CONNECTION_SHEET_CONNECTOR)
				continue;

			if (strncmp(ref_des_prefix, ref_des, ref_des_prefix_len)) {
				printf("Warning: skipping %s sheet connector: "
					"ref. des. prefix don't match\n", ref_des);
				warn_nums++;
				continue;
			}

			if (DBX_OK != TGetFirstSymAttribute(&context, ref_des, &attr)) {
				printf("Warning: skipping %s item: "
					"can't get attribute\n", ref_des);
				warn_nums++;
				continue;
			}

			found.nums++;
			p = realloc(found.items,
				sizeof(typeof(*found.items))*found.nums);
			if (p == NULL) {
				printf("Error: can't allocate memory\n");
				do_exit(-3);
			}
			found.items = p;

			scip = found.items + found.nums-1;
			scip->item = item;
			scip->sheet_num = layer.layerPosition;
			scip->net = "";
			scip->ref_des = "";

			do {
				if (!strcmp("RefDes", attr.type)) {
					scip->ref_des = strdup(attr.value);
				}

				if (!strcmp("Value", attr.type)) {
					/* Value in sheet connector is connected net name */
					scip->net = strdup(attr.value);
				}
			} while (DBX_OK == TGetNextSymAttribute(&context, &attr));
#if 0
printf("DBG: RefDes %s at page %ld with net %s\n", scip->ref_des, scip->sheet_num, scip->net);
#endif

		} while (DBX_OK == TGetNextLayerItem(&context, &item));
	} while (DBX_OK == TGetNextLayer(&context, &layer));

	/* NOTE: sheet connectors are ordered by sheet, but sheets
	 * are NOT ordered */
	for (i = 0; i < found.nums; i++) {
		prev = 0;	/* Sheet 0 cant have items */
		attr_buff_pos = 0;
		attr_buff[0] = '\0';
		ref_des = found.items[i].ref_des;

		for (j = 0; j < found.nums; j++) {
			if (i == j)
				continue;	/* Skip itself */
			if (strcmp(found.items[i].net, found.items[j].net))
				continue;	/* Net is not the same */
			if (prev == found.items[j].sheet_num)
				continue;	/* Skip same sheet number */
			if (prev != 0) {
				attr_buff_pos += sprintf(attr_buff + attr_buff_pos,
					"%s", delimeter);
			}
			attr_buff_pos += sprintf(attr_buff + attr_buff_pos,
					"%ld", found.items[j].sheet_num);
			prev = found.items[j].sheet_num;
			if (attr_buff_pos >= DBX_MAX_ATTRIBUTE_LEN) {
				printf("Error: '%s' attribute length of %s "
					"item will be more than %d",
						sheet_list_attr, ref_des,
						DBX_MAX_ATTRIBUTE_LEN);

				do_exit(-4);
			}
		}

		if (prev == 0) {
			printf("Warning: %s item with net %s has no matched item\n", ref_des, found.items[i].net);
			if (do_highlight) {
				if (DBX_OK != THighlightItem(&context,
					DBX_COLOR_MAGENTA, &(found.items[i].item)))
						printf("\tError: can't highlight\n");
			}
			warn_nums++;
			no_match_nums++;
		}

		if (!get_ref_des_attr_type(&context, ref_des,
					sheet_list_attr, &attr)) {
			printf("Warning: can't get '%s' attribute of %s item\n",
					sheet_list_attr, ref_des);
			warn_nums++;
			continue;
		}
		/* TODO: add prefix and suffix */
		snprintf(attr.value, DBX_MAX_ATTRIBUTE_LEN, " %s ", attr_buff);
		if (DBX_OK != TModifyCompAttribute(&context, ref_des, &attr)) {
			printf("Warning: can't modify '%s' attribute of %s item\n",
					sheet_list_attr, ref_des);
			warn_nums++;
		}
	}

	if (DBX_OK != TCloseDesign(&context, "sch")) {
		printf("Can't close\n");
		do_exit(-5);
	}

	printf("\nFound %d sheet connectors ", found.nums);

	if (no_match_nums > 0)
		printf("(%d not matched) ", no_match_nums);

	if (warn_nums > 0)
		printf("and %d warning(s)\n", warn_nums);
	else
		printf("and no warnings\n");

	if (!do_highlight)
		fprintf(stderr, "Use -H flag to highlight no matched items warnings\n");

	do_exit(0);

	return 0;
}
