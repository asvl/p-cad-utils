#!/usr/bin/python

# Reading csv file from stdin, writing result to stdout.
# For usage use argument --help

# Using:
#	csv-sort-in-cell.py col-name < in.csv > out.csv
#
# Options:
#
# Sort elements in each cell (coma separated elements in cell by default) in
# column specified by name. Can be used to sort RefDes in "natural" order.
#

# TODO: for LaTeX replace
#	'_' by '\_'
#	'~' by '$\sim$'

from __future__ import print_function		# for file= in print()
import sys
import csv
import re
import textwrap
import argparse

def err(s):
	print(s, file = sys.stderr)

def natural_sort(l):
	convert = lambda text: int(text) if text.isdigit() else text.lower()
	alphanum_key = \
		lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
	return sorted(l, key = alphanum_key)

# Compact items with successive numbers
def compact_successive(t):
	r = re.compile("([a-zA-Z]*)([0-9]*)")
	prev = r.match(t[0])

	prev_p = prev.group(1)
	prev_n = prev.group(2)

	f = [t[0]]
	succ = 0
	for i in t[1: ]:
		j = r.match(i)

		if j == None:
			succ = 0
			f.append(prev_p + prev_n)
			prev_p = i
			prev_n = ''
			continue

		if prev_p == j.group(1) and int(prev_n) == int(j.group(2)) - 1:
			# Successive, check next
			succ += 1
			prev_n = j.group(2)
		else:
			if succ > 1:
				f[-1] = f[-1] + '...' + prev_p + prev_n
			if succ == 1:
				f.append(prev_p + prev_n)

			succ = 0
			prev_p = j.group(1)
			prev_n = j.group(2)
			f.append(i)

	# For last element
	if succ > 1:
		f[-1] = f[-1] + '...' + prev_p + prev_n
	if succ == 1:
		f.append(prev_p + prev_n)

	return f

# main

# Parse args
parser = argparse.ArgumentParser(description = 'Sort elements in each cell \
	(coma separated elements in cell by default) in column specified \
	by name. Can be used to sort RefDes in "natural" order. \
	Reading data form stdin and writing to stdout.')

parser.add_argument('-w', '--width',
		help = 'wrap width list for columns, e.g. 10,,0,20')
parser.add_argument('-c', '--compact',
		help = 'compact successive elements in sorted column',
		action = 'store_true')
parser.add_argument('-u', '--utf8',
		help = 'input data is in UTF-8 codepage',
		action = 'store_true')
parser.add_argument('column_name',
		help = 'column name in which cells do the sort')
args = parser.parse_args()

do_compact = args.compact
sort_col_name = args.column_name

# No wrap if list is empty
wrap_width = []
if args.width != None:
	wrap_width = args.width.split(',');

reader = csv.reader(sys.stdin)
writer = csv.writer(sys.stdout)

head = reader.next()

if sort_col_name == None:
	err('Column name for sorting is not specified.')
	err('Header is:\n\t' + ', '.join(head))
	exit(1)

sort_col_num = 0
for s in head:
	if s.strip() == sort_col_name:
		break
	sort_col_num += 1

if sort_col_num == len(head):
	err('Can\'t find "' + sort_col_name + '" in header.')
	err('Header is:\n\t' + ', '.join(head))
	exit(1)

writer.writerow(head)

for row in reader:
	if row == []: continue

	t = map(str.strip, row[sort_col_num].rstrip('.').split(','))
	t = natural_sort(t)

	if do_compact:
		t = compact_successive(t)

	t = ', '.join(t)
	if t != '': t += '.'	# Don't add '.' to empty cell
	row[sort_col_num] = t

	max_wrap_len = 0
	for i, w in enumerate(wrap_width):
		if i >= len(row): break	# wrap_width list is too long
		if w == '' or int(w) == 0:
			# Don't wrap this cell
			continue

		if args.utf8:
			row[i] = row[i].decode("utf8")

		row[i] = textwrap.wrap(row[i], int(w))

		if args.utf8:
			for j in range(0, len(row[i])):
				row[i][j] = row[i][j].encode("utf8")

		# row[i] is list after textwrap
		max_wrap_len = max(max_wrap_len, len(row[i]))

	# Write lists in list by rows
	for i in range(0, max_wrap_len):
		l = []
		for c in row:
			if type(c) is not list:
				# This cell is a text (not list)
				# Copy it at 0 depth
				if i != 0: c = ''
				l.append(c)
				continue

			if i < len(c):
				l.append(c[i])
			else:
				l.append('')

		writer.writerow(l)

	# If no lists in list
	if max_wrap_len == 0:
		writer.writerow(row)

exit(0)
