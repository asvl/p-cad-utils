/* Trim selected lines in P-Cad PCB editor.
	(c) 2017 Sergey Alyoshin <alyoshin.s@gmail.com>
*/

#include <Dbx32.h>	/* P-Cad interface */
#include <stdio.h>
#include <conio.h>
#include <malloc.h>
#include <math.h>
#include <limits.h>
#include <float.h>

DbxContext Context;

struct lines {
	TLine *line;
	unsigned int num;
};

void do_exit(int code)
{
	if (DBX_OK != TCloseDesign(&Context, "pcb"))
		printf("Can't close\n");

	if (code) {
		fprintf(stderr, "\n*** Press key to close ***\n");
		getch();
	}

	exit(code);
}

int intersection_of_lines(TLine const *la, TLine const *lb, TCoord *c)
{
	const long 
		x11 = la->startPt.x,
		y11 = la->startPt.y,
		x12 = la->endPt.x,
		y12 = la->endPt.y,
		x21 = lb->startPt.x,
		y21 = lb->startPt.y,
		x22 = lb->endPt.x,
		y22 = lb->endPt.y;
	double k1, b1, k2, b2, x, y;

	k1 = ((double)y12 - y11)/((double)x12 - x11);
	if (k1 == NAN)
		return 0;
	b1 = y11 - x11*k1;

	k2 = ((double)y22 - y21)/((double)x22 - x21);
	if (k2 == NAN)
		return 0;
	b2 = y21 - x21*k2;
	
	if (k1 == -INFINITY || k1 == INFINITY) {
		x = x11;
		y = k2*x + b2;
	} else if (k2 == -INFINITY || k2 == INFINITY) {
		x = x21;
		y = k1*x + b1;
	} else {
		x = (b2 - b1)/(k1 - k2);
		if (x == NAN)
			return 0;
		y = k1*x + b1;

		if (x > LONG_MAX || x < LONG_MIN
				 || y > LONG_MAX || y < LONG_MIN) {
			printf("Error: borders\n");
			return 0;
		}
	}

	c->x = lround(x);
	c->y = lround(y);

	return 1;
}

double line_lengh(TCoord const *pt, TCoord const *pt2)
{
	return hypot(labs(pt->x - pt2->x), labs(pt->y - pt2->y));
}

int trim_line_to_point(TLine *l, TCoord const *pt)
{
	/* Trim shortest part */

	if (line_lengh(pt, &l->startPt) < line_lengh(pt, &l->endPt)) {
		l->startPt.x = pt->x;
		l->startPt.y = pt->y;
	} else {
		l->endPt.x = pt->x;
		l->endPt.y = pt->y;
	}

	if (DBX_OK != TModifyLine (&Context, l))
		return 0;
	return 1;
}

int main(int argc, char *argv[])
{
	TItem item;
	struct lines lines = {0};
	void *p;
	int i, j;
	TCoord c;

	if (DBX_OK != TOpenDesign(DBX_LANGUAGE, DBX_VERSION, "pcb", &Context)) {
		printf("Can't open PCB\n");
		do_exit(-1);
	}

	if (DBX_OK != TGetFirstSelectedItem(&Context, &item)) {
		printf("Can't get first selected item from PCB\n");
		do_exit(-2);
	}

	/* Collect selected elements */
	do {
/* TODO: DBX_ARC */
		if (item.itemType == DBX_LINE) {
			lines.num++;
			p = realloc(lines.line,
				sizeof(typeof(*lines.line))*lines.num);
			if (p == NULL) {
				printf("Error: can't allocate memory\n");
				do_exit(-3);
			}
			lines.line = p;

			lines.line[lines.num-1] = item.line;
		}
	} while (DBX_OK == TGetNextSelectedItem(&Context, &item));

	if (lines.num == 0) {
		do_exit(0);
	}

	/* Calculate intersections each with each elements */
	for (i = 0; i < lines.num; i++) {
		for (j = i + 1; j < lines.num; j++) {
			if (intersection_of_lines(
					lines.line + i, lines.line + j, &c)) {
				if (!trim_line_to_point(lines.line + i, &c))
					fprintf(stderr,
						"Can't modify line %d\n", i);

				if (!trim_line_to_point(lines.line + j, &c))
					fprintf(stderr,
						"Can't modify line %d\n", j);
			}
		}
	}

	do_exit(0);
	return 0;
}
