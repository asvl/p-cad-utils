/* Place refpoint at center of pads in P-Cad PCB editor.
	(c) 2012 Sergey Alyoshin <alyoshin.s@gmail.com>
*/

/* Usage: select pads and (if exist) refpoint in PCB editor then
   run this program. It will delete selected refpoint, pickpoint and place new
   ones and attributes RefDes, RefDes2, Value at center of selected pads.

   If line is selected then refpoins at the ends and at the center will be
   added. If arc is selected then refpoint will be added at center of the arc.

   Parameters:
   -n		don't place attributes, only refpoint
*/

/* NOTE: can't do this in pattern editor */

#include <Dbx32.h>	/* P-Cad interface */
#include <stdio.h>
#include <conio.h>
#include <malloc.h>
#include <math.h>

DbxContext context;

struct coords {
	TCoord *item;
	unsigned int num;
};

void do_exit(int code)
{
	if (DBX_OK != TCloseDesign(&context, "pcb"))
		printf("Can't close\n");

	if (code) {
		fprintf(stderr, "\n*** Press key to close ***\n");
		getch();
	}

	exit(code);
}

long int center_x_of_uniq(struct coords *pads)
{
	/* Center is calculated only for unique coordinates, good for SOT-23 */

	unsigned int i, j, c_nums;
	long int c = 0;

	c_nums = pads->num;

	for (i = 0; i < pads->num; i++) {
		for (j = 0; j < i; j++) {
			if (pads->item[i].x == pads->item[j].x) {
				c_nums--;
				break;
			}
		}
		if (j == i)
			c += pads->item[i].x;
	}

	if (c_nums)
		c = c/c_nums;

	return c;
}

long int center_y_of_uniq(struct coords *pads)
{
	/* Center is calculated only for unique coordinates, good for SOT-23 */

	unsigned int i, j, c_nums;
	long int c = 0;

	c_nums = pads->num;

	for (i = 0; i < pads->num; i++) {
		for (j = 0; j < i; j++) {
			if (pads->item[i].y == pads->item[j].y) {
				c_nums--;
				break;
			}
		}
		if (j == i)
			c += pads->item[i].y;
	}

	if (c_nums)
		c = c/c_nums;

	return c;
}

void place_attr_type(DbxContext *cont, TAttribute *attr, const char *type)
{
	strncpy(attr->type, type, DBX_MAX_TYPE_LEN);
	attr->value[0] = '\0';

	if (DBX_OK != TPlaceAttribute(cont, attr)) {
		printf("Error: can't add attribute\n");
		do_exit(-3);
	}
}

void place_point(DbxContext *cont, int type, long x, long y)
{
	TPoint point;

	point.pointType = type;
	point.x = x;
	point.y = y;

	if (DBX_OK != TPlacePoint(cont, &point)) {
		printf("Error: can't place point type %d at (%ld; %ld)\n",
				type, x, y);
		do_exit(-3);
	}
}

inline double deg2rad(double degrees)
{
	return degrees*M_PI/180;
}

int main(int argc, char *argv[])
{
	const int MM_TO_DB = 100000;
	TItem item;
	TAttribute attr;
	struct coords pads;
	void *p;
	long x, y, r;
	int add_attrs = 1;

	if (argc > 1 && !strcmp(argv[1], "-n")) {
		add_attrs = 0;
	}

	if (DBX_OK != TOpenDesign(DBX_LANGUAGE, DBX_VERSION, "pcb", &context)) {
		printf("Can't open\n");
		do_exit(-1);
	}

	if (DBX_OK != TGetFirstSelectedItem(&context, &item)) {
		printf("Can't get first selected item\n");
		do_exit(-2);
	}

	/* Collect selected pads coords */
	do {
		if (item.itemType == DBX_PAD) {
			pads.num++;
			p = realloc(pads.item,
				sizeof(typeof(*pads.item))*pads.num);
			if (p == NULL) {
				printf("Error: can't allocate memory\n");
				do_exit(-3);
			}
			pads.item = p;

			pads.item[pads.num-1].x = item.pad.center.x;
			pads.item[pads.num-1].y = item.pad.center.y;
		}

		/* Delete selected refpoint or pickpoint */
		if (item.itemType == DBX_REFPOINT || item.itemType == DBX_PICKPOINT)
			TDeletePoint(&context, &item.point);

		if (item.itemType == DBX_LINE) {
			place_point(&context, DBX_REFPOINT,
					item.line.startPt.x,
					item.line.startPt.y);
			place_point(&context, DBX_REFPOINT,
					item.line.endPt.x,
					item.line.endPt.y);
			place_point(&context, DBX_REFPOINT,
				(item.line.startPt.x + item.line.endPt.x)/2,
				(item.line.startPt.y + item.line.endPt.y)/2);
		}

		if (item.itemType == DBX_ARC) {
			place_point(&context, DBX_REFPOINT,
					item.arc.centerPt.x,
					item.arc.centerPt.y);

			/* Angle is in decidegrees (i.e. degrees*10) */
			r = item.arc.radius;
			x = item.arc.centerPt.x +
				r*cos(deg2rad(0.1*item.arc.startAng));
			y = item.arc.centerPt.y +
				r*sin(deg2rad(0.1*item.arc.startAng));
			place_point(&context, DBX_REFPOINT, x, y);

			x = item.arc.centerPt.x +
				r*cos(deg2rad(0.1*item.arc.sweepAng));
			y = item.arc.centerPt.y +
				r*sin(deg2rad(0.1*item.arc.sweepAng));
			place_point(&context, DBX_REFPOINT, x, y);
		}
	} while (DBX_OK == TGetNextSelectedItem(&context, &item));

	if (!pads.num) {
		do_exit(0);
	}

	/* Place refpoint ant pickpoint */
	x = center_x_of_uniq(&pads);
	y = center_y_of_uniq(&pads);
	place_point(&context, DBX_REFPOINT, x, y);
	place_point(&context, DBX_PICKPOINT, x, y);

	if (!add_attrs)
		do_exit(0);

	/* Add attributes */
	attr.justPoint = DBX_JUSTIFY_CENTER;
	attr.isVisible = 1;
	attr.layerId = DBX_LAYER_TOP_SILK;
	attr.refPoint.x = x;
	attr.refPoint.y = y;
	place_attr_type(&context, &attr, "RefDes");

	attr.isVisible = 0;
	attr.layerId = DBX_LAYER_TOP_ASSY;
	attr.refPoint.x = x;
	attr.refPoint.y = y + 1.27*MM_TO_DB;
	place_attr_type(&context, &attr, "RefDes2");

	attr.refPoint.y = y - 1.27*MM_TO_DB;
	place_attr_type(&context, &attr, "Value");

	do_exit(0);
	return 0;
}
