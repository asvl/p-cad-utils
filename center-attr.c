/* Center selected attributes of components in P-Cad PCB editor.
	(c) 2014 Sergey Alyoshin <alyoshin.s@gmail.com>
*/

/* Usage: select attributes and call this program. */

/* NOTE: can't do this in pattern editor and not work with RefDes (but do work
   with e.g. RefDes2) */

#include <stdio.h>
#include <conio.h>
#include <malloc.h>
#include <Dbx32.h>	/* P-Cad interface */

DbxContext context;

struct coords {
	TCoord *item;
	unsigned int num;
};

void do_exit(int code)
{
	if (DBX_OK != TCloseDesign(&context, "pcb"))
		printf("Can't close\n");

	if (code) {
		fprintf(stderr, "\n*** Press key to close ***\n");
		getch();
	}

	exit(code);
}

long int center_x_of_uniq(struct coords *pads)
{
	/* Center is calculated only for unique coordinates, good for SOT-23 */

	unsigned int i, j, c_nums;
	long int c = 0;

	c_nums = pads->num;

	for (i = 0; i < pads->num; i++) {
		for (j = 0; j < i; j++) {
			if (pads->item[i].x == pads->item[j].x) {
				c_nums--;
				break;
			}
		}
		if (j == i)
			c += pads->item[i].x;
	}

	if (c_nums)
		c = c/c_nums;

	return c;
}

long int center_y_of_uniq(struct coords *pads)
{
	/* Center is calculated only for unique coordinates, good for SOT-23 */

	unsigned int i, j, c_nums;
	long int c = 0;

	c_nums = pads->num;

	for (i = 0; i < pads->num; i++) {
		for (j = 0; j < i; j++) {
			if (pads->item[i].y == pads->item[j].y) {
				c_nums--;
				break;
			}
		}
		if (j == i)
			c += pads->item[i].y;
	}

	if (c_nums)
		c = c/c_nums;

	return c;
}

int component_center(DbxContext *cont, TComponent *comp, TCoord *coord)
{
	struct coords pads;
	TPad pad;
	void *p;

	pads.num = 0;
	pads.item = NULL;

	if (DBX_OK != TGetFirstCompPad(cont, comp->refDes, &pad))
		return 0;

	do {
		pads.num++;
		p = realloc(pads.item,
			sizeof(typeof(*pads.item))*pads.num);
		if (p == NULL) {
			printf("Error: can't realloc() memory for pad\n");
			do_exit(-3);
		}
		pads.item = p;

		pads.item[pads.num-1].x = pad.center.x;
		pads.item[pads.num-1].y = pad.center.y;
	} while (DBX_OK == TGetNextCompPad(cont, &pad));

	coord->x = center_x_of_uniq(&pads);
	coord->y = center_y_of_uniq(&pads);

	free(pads.item);

	return 1;
}

int main(int argc, char *argv[])
{
	TComponent comp;
	TItem item;
	TAttribute attr;
	TCoord coord;
	struct {
		long *itemIds;
		long num;
	} attrs;
	unsigned int i;
	void *p;
	int ret = 0;

	fprintf(stderr, "Move selected attributes to component centers\n");

	if (DBX_OK != TOpenDesign(DBX_LANGUAGE, DBX_VERSION, "pcb", &context)) {
		printf("Can't open\n");
		do_exit(-1);
	}

	/* Get selected attributes */
	if (DBX_OK != TGetFirstSelectedItem(&context, &item)) {
		printf("Can't get first selected item\n");
		do_exit(-2);
	}

	attrs.num = 0;
	attrs.itemIds = NULL;
	do {
		if (item.itemType != DBX_ATTR)
			continue;

		attrs.num++;

		p = realloc(attrs.itemIds,
			sizeof(typeof(*attrs.itemIds))*attrs.num);
		if (p == NULL) {
			printf("Error: can't realloc() for attrs.itemIds\n");
			do_exit(-3);
		}

		attrs.itemIds = p;
		attrs.itemIds[attrs.num - 1] = item.attribute.itemId;
	} while (DBX_OK == TGetNextSelectedItem(&context, &item));

	/* Get all components and find selected attributes */
	if (DBX_OK != TGetFirstComponent(&context, &comp)) {
		printf("Can't get first component\n");
		do_exit(-2);
	}

	do {
		if (DBX_OK != TGetFirstCompAttribute(&context,
					comp.refDes, &attr))
			continue;

		do {
			for (i = 0; i < attrs.num; i++) {
				if (attr.itemId != attrs.itemIds[i])
					continue;

				if (!component_center(&context,
							&comp, &coord)) {
					coord.x = comp.refPoint.x;
					coord.y = comp.refPoint.y;
				}

				/* Modify attribute */
				attr.refPoint = coord;
				if (DBX_OK != TModifyCompAttribute(&context,
							comp.refDes, &attr)) {
					printf("Can't modify attribute "
						"\"%s\"=\"%s\" for %s\n",
							attr.type, attr.value,
							comp.refDes);
					ret = 1;
				}
				break;
			}
		} while (DBX_OK == TGetNextCompAttribute(&context, &attr));
	} while (DBX_OK == TGetNextComponent(&context, &comp));

	do_exit(ret);
	return 0;
}
