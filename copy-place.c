/* Copy selected component place (coords, angle and side) from one board to
   another, swap two selected components or place components with specified
   shift.

	(c) 2012, 2014 Sergey Alyoshin <alyoshin.s@gmail.com>

	TODO:
	1. Copy multiply selected components
*/

#include <stdio.h>
#include <string.h>
#include <conio.h>
#include <ctype.h>
#include <getopt.h>
#include <libgen.h>
#include <limits.h>
#include <assert.h>
#include <Dbx32.h>		/* P-Cad interface */

void do_exit(int code)
{
	while (kbhit()) getch();
	printf("\n*** Press key to close ***\n");
	getch();
	exit(code);
}

int open_design(DbxContext *context)
{
	if (DBX_OK != TOpenDesign(DBX_LANGUAGE, DBX_VERSION, "pcb", context)) {
		printf("Can't open PCB design\n");

		return 0;
	}

	return 1;
}

int close_design(DbxContext *context)
{
	if (DBX_OK != TCloseDesign(context, "pcb")) {
		printf("Can't close PCB design\n");

		return 0;
	}

	return 1;
}

long int double2long(double x)
{
	assert(x >= LONG_MIN - 0.5);
	assert(x <= LONG_MAX + 0.5);
	if (x >= 0)
		return (long)(x + 0.5);

	return (long)(x - 0.5);
}

long int mm2db(double mm)
{
	return double2long(mm*100000.0);
}

long int mil2db(double mil)
{
	return double2long(mil*2540.0);
}

struct opts {
	int is_shift_mode;
	int in_mils;	/* mm by default */
	float x, y;
};

int parse_options(int argc, char *argv[], struct opts *opts);

int main(int argc, char *argv[])
{
	DbxContext context;
	TItem item, item2, item3;
	TComponent src_comp;
	struct opts opts;

	argv[0] = basename(argv[0]);	/* For getopt */

	memset(&opts, 0, sizeof(struct opts));
	if (parse_options(argc, argv, &opts))
		do_exit(-1);

	printf("Copy placement of selected components\n"
		"*** Press X to exit ***\n");

	if (opts.is_shift_mode) {
		if (opts.in_mils)
			printf("\nShift by: %f %f mils\n", opts.x, opts.y);
		else
			printf("\nShift by: %f %f mm\n", opts.x, opts.y);
	}

while (1) {
	int has_source = 0;
	int do_swap = 0;

	while (!has_source) {
		while (kbhit()) getch();
		if (!opts.is_shift_mode)
			printf("\nSelect BASE component and press a KEY or\n"
				"select TWO components to swap\n");
		else
			printf("\nSelect BASE component and press a KEY or\n"
				"select TWO components to shift\n");

		if (tolower(getch()) == 'x')
			exit(0);

		if (!open_design(&context))
			continue;

		if (DBX_OK != TGetFirstSelectedItem(&context, &item)) {
			printf("Can't get selected item\n");
		} else {
			/* Get placement parameters */
			if (item.itemType != DBX_COMPONENT) {
				printf("Selected item is not a component\n");
				goto close_src;
			}
			src_comp.rotationAng = item.component.rotationAng;
			src_comp.isFlipped = item.component.isFlipped;
			src_comp.refPoint = item.component.refPoint;

			/* Check if selected more than one item */
			if (DBX_OK == TGetNextSelectedItem(&context, &item2)) {
				if (item2.itemType != DBX_COMPONENT) {
					printf("Selected second item is not a component\n");
					goto close_src;
				}

				if (DBX_OK == TGetNextSelectedItem(&context, &item3)) {
					/* More than two items are selected, can't swap */
					printf("Three or more items are selected, don't know what to do\n");
					goto close_src;
				}
				do_swap = 1;

				if (!opts.is_shift_mode) {
					/* Rote item (parameters stored in src_comp) */
					TRotateComponent(&context,
						item2.component.rotationAng - item.component.rotationAng,
						&(TCoord){-1, -1}, &item.component);
				}

				TRotateComponent(&context,
						src_comp.rotationAng - item2.component.rotationAng,
						&(TCoord){-1, -1}, &item2.component);

				/* Move item (parameters stored in src_comp) */
				if (!opts.is_shift_mode) {
					TMoveComponent(&context,
						item2.component.refPoint.x - item.component.refPoint.x,
						item2.component.refPoint.y - item.component.refPoint.y,
						&item.component);
				}

				if (!opts.is_shift_mode) {
					TMoveComponent(&context,
						src_comp.refPoint.x - item2.component.refPoint.x,
						src_comp.refPoint.y - item2.component.refPoint.y,
						&item2.component);
				} else {
					long x, y;

					if (opts.in_mils) {	/* mils */
						x = mil2db(opts.x);
						y = mil2db(opts.y);
					} else {		/* mm */
						x = mm2db(opts.x);
						y = mm2db(opts.y);
					}

					TMoveComponent(&context,
						src_comp.refPoint.x - item2.component.refPoint.x + x,
						src_comp.refPoint.y - item2.component.refPoint.y + y,
						&item2.component);
				}

				/* Swap item and item2 */
				if (item.component.isFlipped ^ item2.component.isFlipped) {
					if (!opts.is_shift_mode)
						TFlipComponent(&context, &(TCoord){-1, -1}, &item.component);

					TFlipComponent(&context, &(TCoord){-1, -1}, &item2.component);
				}
			}

			has_source = 1;
		}
close_src:
		if (!close_design(&context)) {
			printf("Fatal error!\n");
			do_exit(-1);
		}
	}

	int has_dest = 0;
	while (!has_dest && !do_swap) {
		while (kbhit()) getch();
		printf("Select component to MOVE and press a KEY...\n");
		if (tolower(getch()) == 'x')
			exit(0);

		if (!open_design(&context))
			continue;

		if (DBX_OK != TGetFirstSelectedItem(&context, &item)) {
			printf("Can't get selected item\n");
		} else {
			/* Set placement parameters */
			if (item.itemType != DBX_COMPONENT) {
				printf("Selected item is not a component\n");
				goto close_dst;
			}
			has_dest = 1;

			/* If isFlipped? */
			if (src_comp.isFlipped ^ item.component.isFlipped)
				TFlipComponent(&context, &(TCoord){-1, -1}, &item.component);

			TRotateComponent(&context,
					src_comp.rotationAng - item.component.rotationAng,
					&(TCoord){-1, -1}, &item.component);

			TMoveComponent(&context,
					src_comp.refPoint.x - item.component.refPoint.x,
					src_comp.refPoint.y - item.component.refPoint.y,
					&item.component);

		}
close_dst:
		if (!close_design(&context)) {
			printf("Fatal error!\n");
			do_exit(-1);
		}
	}

#if 0 /* Multiply items */
	do {
		found_nums++;

		if (item.itemType != DBX_SYMBOL) {
			skipped_nums++;
			continue;
		}
		ref_des = item.symbol.refDes;

		/* Truncate by ':' */
		p = strpbrk(ref_des, ":");
		if (p != NULL)
			*p = '\0';

		if (DBX_OK != TGetFirstCompPin(&context, ref_des, &pin)) {
			fprintf(stderr, "Warning: can't get pin of symbol %s\n", ref_des);
			skipped_nums++;
			continue;
		}

		printf("%s\n", ref_des);
		do {
			if (pin.netId == 0) {
				strcpy(net.netName, "(not connected)");
			} else if (DBX_OK != TGetNetById(&context, pin.netId, &net)) {
				fprintf(stderr,
					"Warning: can't get net by ID%ld of pin %s\n",
					pin.netId, pin.compPin.pinDes);
				continue;
			}
			printf("\t%s\t%s\n", pin.compPin.pinDes, net.netName);
		} while (DBX_OK == TGetNextCompPin(&context, &pin));

		printf("\n");
	} while (DBX_OK == TGetNextSelectedItem(&context, &item));
#endif

}

	do_exit(0);

	return 0;
}

int parse_options(int argc, char *argv[], struct opts *opts)
{
	/* Program run options */

	int o, ret = 0;
	const char short_opt[] =
		"hmx:y:";
	const struct option long_opt[] = {
		{"help",	no_argument,		NULL, 'h'},
		{"mils",	no_argument,		NULL, 'm'},
		{"x",		required_argument,	NULL, 'x'},
		{"y",		required_argument,	NULL, 'y'},
		{NULL,		0,			NULL, 0}
	};

	do {
		o = getopt_long(argc, argv, short_opt, long_opt, NULL);
		switch (o) {
		case 'h':	/* Help */

			printf("Copy selected component place (coords, angle and side) from one board\n"
				"to another, swap two selected components or place components with\n"
				"specified shift.\n");
			printf("Options:\n"
				"--help, -h	this help\n"
				"--mils, -m	x and y is in mils\n"
				"--x, -x	x-axis shift\n"
				"--y, -y	y-axis shift\n"
				"\n");

			exit(EXIT_SUCCESS);
		case 'm':
			opts->in_mils = 1;
			break;
		case 'x':
			if (1 != sscanf(optarg, "%f", &opts->x)) {
				fprintf(stderr, "can't parse %s\n", optarg);
				ret = 1;
			}
			opts->is_shift_mode = 1;
			break;
		case 'y':
			if (1 != sscanf(optarg, "%f", &opts->y)) {
				fprintf(stderr, "can't parse %s\n", optarg);
				ret = 1;
			}
			opts->is_shift_mode = 1;
			break;
		};
	} while (o != -1);

	return ret;
}
