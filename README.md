Various P-CAD utilites


Building
--------

1. Install mingw compiler

2. Get P-CAD DBX header file Dbx32.h and .dll file Dbx32.dll

3. Check PATH, INCDIR and LIBDIR varialbes in Makefile

3. Run make
	C:\MinGW\bin\mingw32-make.exe


Running
-------

1. Open Sch or Pcb (not work in symbol editor, pad editor and library
   executive)

2. Run utilite

3. You can add menu item for util in Utils/Customize


License
-------

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.

