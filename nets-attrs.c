/* Print nets on pins for selected components in sch or pcb.

	(c) 2012, 2014, 2016 Sergey Alyoshin <alyoshin.s@gmail.com>
*/

/*
	TODO:

	* Fix several gates component report

	* Report if net is highlighted

	* Generate .ECO for nets of elements as attributes with prefix
	  for connectors
*/

#include <stdio.h>
#include <string.h>
#include <conio.h>
#include <Dbx32.h>	/* P-Cad interface */

unsigned int g_skipped_nums = 0;

void do_exit(int code)
{
	fprintf(stderr, "\n*** Press key to close ***\n");
	getch();
	exit(code);
}

void report_nets(DbxContext *context, const char *ref_des, int in_sch)
{
	TPin pin;
	TPad pad;
	TNet net;
	long int net_id;

	printf("%s nets:\n", ref_des);

	if (in_sch) {
		if (DBX_OK != TGetFirstCompPin(context, ref_des, &pin)) {
			fprintf(stderr, "Warning: can't get pin of symbol %s\n", ref_des);
			g_skipped_nums++;

			return;
		}
	} else {
		if (DBX_OK != TGetFirstCompPad(context, ref_des, &pad)) {
			fprintf(stderr, "Warning: can't get pad of component %s\n", ref_des);
			g_skipped_nums++;

			return;
		}
	}

	do {
		if (in_sch)
			net_id = pin.netId;
		else
			net_id = pad.netId;

		if (net_id == 0) {
			strcpy(net.netName, "(not connected)");
		} else if (DBX_OK != TGetNetById(context, net_id, &net)) {
			if (in_sch) {
				fprintf(stderr,
					"Warning: can't get net by ID%ld of pin %s\n",
					net_id, pin.compPin.pinDes);
			} else {
				fprintf(stderr,
					"Warning: can't get net by ID%ld of pad %s\n",
					net_id, pad.compPin.pinDes);
			}

			continue;
		}
		printf("\t%s\t%s\t%s\n",
			(in_sch? pin.compPin.pinDes: pad.compPin.pinDes),
			(in_sch? pin.compPin.pinName: pad.compPin.pinName),
			net.netName);
	} while (in_sch? (DBX_OK == TGetNextCompPin(context, &pin)):
			 (DBX_OK == TGetNextCompPad(context, &pad)));

	printf("\n");
}

void report_attrs(DbxContext *context, const char *ref_des, int in_sch,
		int in_eco_format, int is_visible)
{
	TAttribute attr;

	printf("%s attributes:\n", ref_des);
	if ((in_sch && DBX_OK != TGetFirstSymAttribute(context, ref_des, &attr)) ||
	   (!in_sch && DBX_OK != TGetFirstCompAttribute(context, ref_des, &attr))) {
		fprintf(stderr, "Can't get first attribute for component %s\n", ref_des);
		g_skipped_nums++;

		return;
	}

	do {
		if (!in_eco_format) {
			if (!is_visible)
				printf("\t%s\t%s\n", attr.type, attr.value);
			else
				printf("%s\t%s\t%s\n",
						attr.isVisible? "Vis": "Inv",
						attr.type, attr.value);
		} else
			printf("CompAttrModify \"%s\" \"%s\" \"%s\"\n",
					ref_des, attr.type, attr.value);

	} while ((in_sch && DBX_OK == TGetNextSymAttribute(context, &attr)) ||
		(!in_sch && DBX_OK == TGetNextCompAttribute(context, &attr)));

	printf("\n");
}

int main(int argc, char *argv[])
{
	DbxContext context;
	TItem item;
	TComponent component;
	char *ref_des, *p;
	unsigned int found_nums = 0;
	int in_sch, reporting_nets = 0, reporting_attrs = 0, reporting_eco = 0,
		reporting_attr_is_visible = 0;
	int i;

	fprintf(stderr, "Printing nets or attributes of selected components in sch or pcb\n\n");

	for (i = 1; i < argc; i++) {
		if (!strcmp("-h", argv[i])) {
			printf("Usage:\n"
				"	-h	this help\n"
				"	-n	report nets (by default)\n"
				"	-a	report attributes\n"
				"	-v	report if attribute is visible\n"
				"	-e	generate .eco with attributes\n"
				);
			exit(0);
		}
		if (!strcmp("-n", argv[i])) {
			reporting_nets = 1;
		}
		if (!strcmp("-a", argv[i])) {
			reporting_attrs = 1;
		}
		if (!strcmp("-v", argv[i])) {
			reporting_attr_is_visible = 1;
		}
		if (!strcmp("-e", argv[i])) {
			reporting_eco = 1;
		}
	}

	/* Reporting nets by default */
	if (!reporting_nets && !reporting_attrs && !reporting_eco)
		reporting_nets = 1;

	in_sch = 1;
	if (DBX_OK != TOpenDesign(DBX_LANGUAGE, DBX_VERSION, "sch", &context)) {
		in_sch = 0;
		if (DBX_OK != TOpenDesign(DBX_LANGUAGE, DBX_VERSION, "pcb", &context)) {
			fprintf(stderr, "Can't open sch or pcb design\n");
			do_exit(-1);
		}
	}

	if (DBX_OK != TGetFirstSelectedItem(&context, &item)) {
		fprintf(stderr, "Can't get selected items\n");

		/* If none item is selected, show for all components in sch */
		if (DBX_OK == TGetFirstComponent(&context, &component)) {
			do {
				found_nums++;

				ref_des = component.refDes;

				/* Truncate by ':' */
				p = strpbrk(ref_des, ":");
				if (p != NULL)
					*p = '\0';

				if (reporting_nets)
					report_nets(&context, ref_des, in_sch);

				if (reporting_attrs)
					report_attrs(&context, ref_des, in_sch, 0,
							reporting_attr_is_visible);
				if (reporting_eco)
					report_attrs(&context, ref_des, in_sch, 1, 0);

			} while (DBX_OK == TGetNextComponent(&context, &component));
		}

		if (DBX_OK != TCloseDesign(&context, ""))
			fprintf(stderr, "Can't close\n");
		do_exit(-2);
	} else {
		do {
			found_nums++;

			if (item.itemType != DBX_SYMBOL && item.itemType != DBX_COMPONENT) {
				g_skipped_nums++;
				continue;
			}

			if (in_sch)
				ref_des = item.symbol.refDes;
			else
				ref_des = item.component.refDes;

			/* Truncate by ':' */
			p = strpbrk(ref_des, ":");
			if (p != NULL)
				*p = '\0';

			if (reporting_nets)
				report_nets(&context, ref_des, in_sch);

			if (reporting_attrs)
				report_attrs(&context, ref_des, in_sch, 0,
						reporting_attr_is_visible);

			if (reporting_eco)
				report_attrs(&context, ref_des, in_sch, 1, 0);

		} while (DBX_OK == TGetNextSelectedItem(&context, &item));
	}

	if (DBX_OK != TCloseDesign(&context, "")) {
		fprintf(stderr, "Can't close\n");
		do_exit(-5);
	}

	if (!g_skipped_nums)
		fprintf(stderr, "Found %d items\n", found_nums);
	else
		fprintf(stderr, "Found %d items (%d skipped)\n",
				found_nums, g_skipped_nums);
	do_exit(0);
	return 0;
}
