# Build with MinGW

PATH:= C:/MinGW/bin
INCDIR:= C:/Program Files/P-CAD 2006/Dbx/Dbxsrc/Common
LIBDIR:= C:/Program Files/P-CAD 2006/Dbx

CFLAGS:= -g

ALL:= \
	center-attr.exe \
	copy-place.exe \
	nets-attrs.exe \
	refpoint.exe \
	trim.exe \
	xref.exe \

all: $(ALL)

clean:
	$(RM) $(ALL)

%.exe: %.c
	gcc -o$@ $(CFLAGS) -Wl,--enable-stdcall-fixup -Wall -I"$(INCDIR)" "$(LIBDIR)/Dbx32.dll" $^
